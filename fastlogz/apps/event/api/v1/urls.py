from django.urls import include, path

from fastlogz.apps.log.api.v1.views import (
    DriverStatusDataList,
    DvirDataList,
    GeneralDataList,
    SignDataList,
)

from . import views

app_name = 'event'

live = [
    path('', views.LiveDataView.as_view()),
    path('short/', views.LiveDataShortView.as_view()),
    path('long/<int:pk>/', views.LiveDataLongView.as_view())
]


logs_url = [
    path('limits/', views.DriverTimeLimits.as_view()),
    path('share/', views.ShareLiveDataApiView.as_view()),
    path('eld_confirmation/', views.EldConfimationApiView.as_view()),
    path('apk_version/', views.ApkVersionApiView.as_view()),
    path('local_data/', views.LocalEventDataApiView.as_view()),
    path('sign/', views.SignApiView.as_view()),
    path('dvir/', views.DVIRApiView.as_view()),
    path('general_info/', views.GeneralMainApiView.as_view()),
    path('general/<int:id>', views.GeneralMainRetriveApiView.as_view()),
    path('trailer_of_driver/', views.TrailerCreateListView.as_view()),
    path('co_driver/', views.CoDriverListView.as_view()),
    path('vehicle_of_driver/', views.DriverVehicleListView.as_view()),
    path('poweron/', views.PowerOnView.as_view()),
    path('newtime/', views.NewTimeView.as_view()),
    path('enginecache/', views.EngineCacheView.as_view()),
    path('motion/', views.MotionView.as_view()),
    path('buffer/', views.BufferView.as_view()),
    path('live/', include(live)),
    path('driverbehavior/', views.DriverBehaviorView.as_view()),
    path('emission/', views.EmissionView.as_view()),
    path('enginelive/', views.EngineLiveView.as_view()),
    path('fuel/', views.FuelView.as_view()),
    path('transmission/', views.TransmissionView.as_view()),
    path('status/', views.DriverStatusView.as_view()),
    path('status/filter/', views.DriverStatusFilterView.as_view()),
    path('status/edit/<int:id>', views.DriverStatusEditApiView.as_view()),
    path('inspect/', views.InspectionPdfApiView.as_view()),


]

datalist = [
    path('general_last/', GeneralDataList.as_view()),
    path('sign_last/', SignDataList.as_view()),
    path('dvir_last/', DvirDataList.as_view()),
    path('logs_last/', DriverStatusDataList.as_view()),

]

logs = [
    path('reported_eld_data/', views.ReportedFMCSA.as_view())
]

urlpatterns = [
    path('', include(logs_url)),
    path('logs/', include(logs)),
    path('', include(datalist))

]
