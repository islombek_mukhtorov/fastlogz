from email.message import EmailMessage

from django.conf import settings
from django.utils import timezone
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.views import APIView

from fastlogz.apps.eld import models
from fastlogz.apps.event import models as md
from fastlogz.apps.event import serializers as sr
from fastlogz.apps.event.serializers import (
    DriverLimitSerializer,
    DriverStatusSerializer2,
    InspectionPdfSer,
    LiveDataGetSerializer,
    LiveDataSerializer,
    LiveDataShortSerializer,
    LocalEventShortSerializer,
)
from fastlogz.apps.log.models import LogsModels


class ReportedFMCSA(APIView):
    def get(self, request):
        user_id = models.Company.objects.get(email=request.user).id
        eld = models.ELD.objects.filter(company_id=user_id)
        context = []
        for x in eld:
            context.append(str(x))
        return Response({"status": f"{context}"})


def response_post(model, serializer, request=None):
    serialized = serializer(data=request.data)
    serialized.Meta.model = model
    if serialized.is_valid():
        print(serialized.errors)
        serialized.save()
        print(serialized.data)
        return Response(serialized.data)
    else:
        print(serialized.data)
        print(serialized.errors)
        return Response(serialized.errors)


def response_get(model, serializer, request=None):
    serialized = serializer(request.data, many=True)
    serialized.Meta.model = model
    return Response(serialized.data)


class PowerOnView(APIView):
    queryset = md.PowerOn.objects.all()

    def post(self, request):
        return response_post(md.PowerOn, sr.MainEventSerializer, request)


class NewTimeView(APIView):
    queryset = md.NewTime.objects.all()

    def get(self, request):
        return response_get(sr.MainEventSerializer, model=md.NewTime)

    def post(self, request):
        return response_post(md.NewTime, sr.MainEventSerializer, request)


class EngineCacheView(APIView):
    queryset = md.EngineCache.objects.all()

    def post(self, request):
        return response_post(md.EngineCache, sr.MainEventSerializer, request)


class MotionView(APIView):
    queryset = md.MotionPeriodic.objects.all()

    def get(self, request):
        return Response({"data": "motion"})

    def post(self, request):
        return response_post(md.MotionPeriodic, sr.MainEventSerializer, request)


class BufferView(APIView):
    queryset = md.BufferRecord.objects.all()

    def post(self, request):
        return response_post(md.BufferRecord, sr.MainEventSerializer, request)


# LIVEDATA is starting
class LiveDataView(APIView):
    queryset = md.LiveData.objects.all()

    def get(self, request):
        a = md.LiveData.objects.order_by("driver", "-id").distinct("driver")
        serialized = LiveDataSerializer(a, many=True)
        return Response(serialized.data)

    def post(self, request):
        request.data["driver"] = models.DRIVERS.objects.get(email=request.user).id
        request.data["truck"] = models.DRIVERS.objects.get(
            email=request.user
        ).vehicle_id.id
        return response_post(md.LiveData, sr.MainEventSerializer, request)


class LiveDataShortView(APIView):
    queryset = md.LiveData.objects.all()

    def get(self, request):
        a = md.LiveData.objects.order_by("driver", "-id").distinct("driver")
        serialized = LiveDataShortSerializer(a, many=True)
        return Response(serialized.data)

    def post(self, request):
        request.data["driver"] = models.DRIVERS.objects.get(email=request.user).id
        return response_post(md.LiveData, sr.MainEventSerializer, request)


class LiveDataLongView(APIView):
    queryset = md.LiveData.objects.all()

    def get(self, request, pk):
        a = md.LiveData.objects.get(id=pk)
        serialized = LiveDataGetSerializer(a)
        return Response(serialized.data)


class DriverBehaviorView(APIView):
    queryset = md.DriverBehavior.objects.all()

    def post(self, request):
        return response_post(md.DriverBehavior, sr.MainEventSerializer, request)


class EmissionView(APIView):
    queryset = md.Emission.objects.all()

    def get(self, request):
        return Response({"data": "Emission"})

    def post(self, request):
        return response_post(md.Emission, sr.MainEventSerializer, request)


class EngineLiveView(APIView):
    queryset = md.EngineRecordLive.objects.all()

    def get(self, request):
        return Response({"data": "Engine Live"})

    def post(self, request):
        return response_post(md.EngineRecordLive, sr.MainEventSerializer, request)


class FuelView(APIView):
    queryset = md.FuelRecord.objects.all()

    def get(self, request):
        return Response({"data": "motion"})

    def post(self, request):
        return response_post(md.FuelRecord, sr.MainEventSerializer, request)


class TransmissionView(APIView):
    queryset = md.Transmission.objects.all()

    def get(self, request):
        return Response({"data": "Transmission"})

    def post(self, request):
        return response_post(md.Transmission, sr.MainEventSerializer, request)


class DriverStatusView(generics.ListCreateAPIView):
    serializer_class = sr.DriverStatusSerializer

    queryset = md.DriverStatus.objects.all()

    def perform_create(self, serializer):
        driver = models.DRIVERS.objects.get(email=self.request.user)
        last_live = md.LiveData.objects.filter(
            driver=driver, truck=driver.vehicle_id
        ).last()
        logs = LogsModels.objects.filter(
            driver=driver, vehicle=driver.vehicle_id
        ).last()
        if (
            LogsModels.objects.filter(driver=driver, vehicle=driver.vehicle_id).exists()
            and logs.created.date() == timezone.now().date()
        ):
            pass
        else:
            if logs is None:
                logs = LogsModels.objects.create(
                    driver=driver,
                    vehicle=driver.vehicle_id,
                )
            else:
                last_logs = LogsModels.objects.filter(
                    driver=driver, vehicle=driver.vehicle_id
                ).last()
                logs = LogsModels.objects.create(
                    driver=driver,
                    vehicle=driver.vehicle_id,
                    shift=last_logs.shift,
                    cycle=last_logs.cycle,
                    untilbreak=last_logs.untilbreak,
                    driving_limit=last_logs.driving_limit,
                )

        serializer.save(
            driver=driver,
            vehicle=driver.vehicle_id.vehicle_id,
            odometer=last_live.odometer if last_live is not None else 0,
            engine_hours=last_live.engine_hours,
            logs=logs,
        )
        return super().perform_create(serializer)


class DriverStatusFilterView(generics.ListAPIView):
    serializer_class = sr.DriverStatusSerializer
    queryset = md.DriverStatus.objects.all()

    def get(self, request, *args, **kwargs):
        if request.GET["driver_id"] and request.GET["log_id"]:
            driverid = request.GET["driver_id"]
            log_id = request.GET["log_id"]
            driver = models.DRIVERS.objects.get(id=driverid)
            logs = LogsModels.objects.get(id=log_id)
            status = md.DriverStatus.objects.filter(driver=driver, logs=logs).order_by(
                "cr_time"
            )
            if status.last() is not None:
                last = status.last()
                last.dur = (timezone.now() - last.cr_time).total_seconds()
                last.save()
            serializer = DriverStatusSerializer2(status, many=True)
            return Response(serializer.data)


class DriverTimeLimits(generics.ListAPIView):
    serializer_class = sr.DriverStatusSerializer
    queryset = md.DriverStatus.objects.all()

    def get(self, request, *args, **kwargs):
        driver = models.DRIVERS.objects.get(email=request.user.email)
        logs = LogsModels.objects.filter(
            driver=driver, vehicle=driver.vehicle_id
        ).last()
        if logs is not None and logs.created.date() == timezone.now().date():
            pass
        else:
            if logs is None:
                logs = LogsModels.objects.create(
                    driver=driver,
                    vehicle=driver.vehicle_id,
                )
            else:
                last_logs = LogsModels.objects.filter(
                    driver=driver, vehicle=driver.vehicle_id
                ).last()
                logs = LogsModels.objects.create(
                    driver=driver,
                    vehicle=driver.vehicle_id,
                    shift=last_logs.shift,
                    cycle=last_logs.cycle,
                    untilbreak=last_logs.untilbreak,
                    driving_limit=last_logs.driving_limit,
                )
        # status = md.DriverStatus.objects.filter(driver=driver, logs=logs)
        worked_status = md.DriverStatus.objects.filter(
            driver=driver, logs=logs, status__in=["ON", "D"]
        )
        if worked_status is not None:
            if logs.worked_hours is None:
                logs.worked_hours = 0
            for sts in worked_status:
                logs.worked_hours += sts.dur
        logs.save()
        serializer = DriverLimitSerializer(logs, read_only=True)
        return Response(serializer.data)


class DriverVehicleListView(generics.ListAPIView):
    serializer_class = sr.DriverVehicleSerializer
    queryset = models.Vehicle.objects.all()

    def get_queryset(self):
        driver = models.DRIVERS.objects.get(email=self.request.user.email)
        queryset = models.Vehicle.objects.filter(id=driver.vehicle_id.id)
        return queryset


class CoDriverListView(generics.ListAPIView):
    serializer_class = sr.DriverGetSerializer
    queryset = models.DRIVERS.objects.all()

    def filter_queryset(self, queryset):
        driver = models.DRIVERS.objects.get(email=self.request.user.email)
        if driver.co_driver is not None:
            queryset = models.DRIVERS.objects.filter(id=driver.co_driver.id)
        else:
            queryset = models.DRIVERS.objects.filter(name="aaaaaaaaaa")
        return super().filter_queryset(queryset)


class TrailerCreateListView(generics.ListCreateAPIView):
    serializer_class = sr.TrailerSerializer
    queryset = models.Trailer.objects.all()

    def get_queryset(self):
        driver = models.DRIVERS.objects.get(email=self.request.user.email)
        queryset = driver.trail_number.all()
        return queryset

    def perform_create(self, serializer):
        driver = models.DRIVERS.objects.get(email=self.request.user.email)
        company = models.Company.objects.get(id=driver.company.id)
        serializer.save(company=company)
        return super().perform_create(serializer)


class GeneralMainApiView(generics.ListCreateAPIView):
    serializer_class = sr.GeneralMainSerializer
    queryset = md.GeneralMain.objects.all()

    def perform_create(self, serializer):
        driver = models.DRIVERS.objects.get(email=self.request.user.email)
        log = LogsModels.objects.filter(
            driver=driver, date_cr=serializer.validated_data["day"]
        ).last()
        serializer.save(driver=driver)
        if log is not None:
            serializer.save(logs=log, driver=driver)
        return super().perform_create(serializer)

    def get(self, request, *args, **kwargs):
        log_id = request.GET["log_id"]
        logs = LogsModels.objects.filter(id=log_id).last()
        general = md.GeneralMain.objects.filter(logs=logs).last()
        if general is not None:
            serializer = sr.GeneralMainSerializer(general, read_only=True)
            return Response(serializer.data)
        else:
            return Response({"data": "driver has not GeneralInfo for this log!"})


class GeneralMainRetriveApiView(generics.RetrieveUpdateAPIView):
    serializer_class = sr.GeneralMainSerializer
    queryset = md.GeneralMain.objects.all()
    lookup_field = "id"


class DVIRApiView(generics.ListCreateAPIView):
    serializer_class = sr.DVIRSerializer
    queryset = md.DVIR.objects.all()

    def perform_create(self, serializer):
        driver = models.DRIVERS.objects.get(email=self.request.user.email)
        logs = LogsModels.objects.filter(driver=driver).last()
        serializer.save(driver=driver, today_log=logs)
        return super().perform_create(serializer)

    def get(self, request, *args, **kwargs):
        log_id = request.GET["log_id"]
        logs = LogsModels.objects.filter(id=log_id).last()
        dvirs = md.DVIR.objects.filter(today_log=logs)
        serializer = sr.DVIRSerializer2(dvirs, many=True, read_only=True)
        return Response(serializer.data)


class SignApiView(generics.CreateAPIView):
    serializer_class = sr.SignSerializer
    queryset = md.Sign.objects.all()

    def perform_create(self, serializer):
        driver = models.DRIVERS.objects.get(email=self.request.user.email)
        serializer.save(driver=driver)
        return super().perform_create(serializer)


class LocalEventDataApiView(generics.ListCreateAPIView):
    serializer_class = sr.LocalEventDataSerializer
    queryset = md.LocalEventDatas.objects.all()

    def perform_create(self, serializer):
        driver = models.DRIVERS.objects.get(email=self.request.user.email)
        truck = driver.vehicle_id
        shortlocal = md.LocalEventsShort()
        shortlocal.company = driver.company
        shortlocal.driver = driver
        shortlocal.vehicle = truck.vehicle_id
        shortlocal.origin = serializer.validated_data["point"][0]
        shortlocal.destination = serializer.validated_data["point"][-1]
        shortlocal.start_time = serializer.validated_data["moment"][0]
        shortlocal.duration = (
            serializer.validated_data["trip_hours"][-1]
            - serializer.validated_data["trip_hours"][0]
        )
        shortlocal.distance = (
            serializer.validated_data["trip_distance"][-1]
            - serializer.validated_data["trip_distance"][0]
        )
        shortlocal.save()
        serializer.save(driver=driver, truck=truck)
        return super().perform_create(serializer)

    def list(self, request, *args, **kwargs):
        company = md.LocalEventsShort.objects.for_company(user=request.user)
        query = LocalEventShortSerializer(company, many=True, read_only=True)
        return Response(query.data)


class ApkVersionApiView(generics.CreateAPIView):
    serializer_class = sr.ApkVersionSerializer
    queryset = md.ApkVersion.objects.all()

    def perform_create(self, serializer):
        driver = models.DRIVERS.objects.get(email=self.request.user.email)
        serializer.save(driver=driver)
        driver.app_version = serializer.validated_data["apk_version"]
        driver.device_version = serializer.validated_data["mobile_version"]
        driver.save()
        vehicle = models.Vehicle.objects.get(id=driver.vehicle_id.id)
        vehicle.device_version = serializer.validated_data["mobile_version"]
        vehicle.save()
        return super().perform_create(serializer)


class EldConfimationApiView(generics.ListCreateAPIView):
    serializer_class = sr.EldConfimationSerializer
    queryset = md.EldConfimation.objects.all()

    def get(self, request, *args, **kwargs):
        driverid = request.GET["driver_id"]
        driver = models.DRIVERS.objects.get(id=driverid)
        confir = md.EldConfimation.objects.filter(driver=driver).order_by("-id")
        if confir[0].similarity is True:
            return Response({"status": "200", "response": "matched"})
        else:
            return Response({"status": "400", "response": "not matched"})


class ShareLiveDataApiView(generics.ListCreateAPIView):
    serializer_class = sr.ShareLiveDataSerializer
    queryset = md.ShareLiveData.objects.all()

    def get(self, request, *args, **kwargs):
        a = md.ShareLiveData.objects.filter(id=request.GET["pk"])
        serialize = sr.ShareLiveDataSerializer2(a, read_only=True, many=True)
        return Response(serialize.data)


class DriverStatusEditApiView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = sr.DriverStatusSerializer2
    queryset = md.DriverStatus.objects.all()
    lookup_field = "id"

    def get_serializer_context(self):
        return {"id": self.kwargs["id"]}


class InspectionPdfApiView(APIView):
    queryset = md.InspectionPdf.objects.all()

    def get(
        self,
        request,
    ):
        queryset = LogsModels.objects.get(id=id)
        serializer = InspectionPdfSer(queryset, read_only=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        request.data["driver"] = models.DRIVERS.objects.get(email=request.user).id
        serializer = InspectionPdfSer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            email = EmailMessage(
                'Reported Daily Log',
                'Here is reported log document file ',
                settings.EMAIL_HOST_USER,
                [serializer.data['email']]
            )
            up_file = request.FILES['pdf_file']
            email.attach(up_file.name, up_file.read(), up_file.content_type)
            email.send()
            return Response({"data": "file have been sent!"})
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
