# from django.conf import settings
from django.contrib.gis import admin
from leaflet.admin import LeafletGeoAdmin

from .models import (
    DVIR,
    ApkVersion,
    BufferRecord,
    DriverBehavior,
    DriverStatus,
    EldConfimation,
    Emission,
    EngineRecordLive,
    FuelRecord,
    GeneralMain,
    LiveData,
    LocalEventDatas,
    LocalEventsShort,
    MotionPeriodic,
    NewTime,
    PowerOn,
    Sign,
    Transmission,
)


@admin.register(
    LocalEventsShort,
    ApkVersion,
    EldConfimation,
    DriverBehavior,
    FuelRecord,
    BufferRecord,
    PowerOn,
    NewTime,
    MotionPeriodic,
    LiveData,
    Emission,
    EngineRecordLive,
    Transmission,
    DriverStatus,
    GeneralMain,
    DVIR,
    LocalEventDatas,
    Sign,
)
class GeoAdmin(LeafletGeoAdmin):
    pass
    # def has_add_permission(self, request):
    #     return True if settings.DEBUG else False
