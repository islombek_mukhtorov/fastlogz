from django.utils import timezone
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from fastlogz.apps.eld.models import DRIVERS, Trailer, Vehicle
# import schedule
from fastlogz.apps.log.models import LogsModels

from .models import (
    DVIR,
    ApkVersion,
    DriverStatus,
    EldConfimation,
    GeneralMain,
    InspectionPdf,
    LiveData,
    LocalEventDatas,
    LocalEventsShort,
    ShareLiveData,
    Sign,
)


class MainEventSerializer(ModelSerializer):
    class Meta:
        fields = "__all__"

        def __init__(self, model):
            self.model = model


class LiveDataSerializer(ModelSerializer):
    class Meta:
        fields = "__all__"
        model = LiveData


class DriverStatusSerializer(ModelSerializer):
    class Meta:
        fields = ("id", "status", "point", "note", "cr_time")
        model = DriverStatus

    def create(self, validated_data):
        driver = validated_data["driver"]
        logs = LogsModels.objects.filter(
            driver=driver, vehicle=driver.vehicle_id
        ).last()
        instance = self.Meta.model(**validated_data)
        last = DriverStatus.objects.filter(driver=driver, logs=logs).last()
        instance.save()
        last_sts = DriverStatus.objects.filter(driver=driver, logs=logs).last()
        first = DriverStatus.objects.filter(driver=driver, logs=logs).first()
        last_dvir = DVIR.objects.filter(
            driver=driver,
            created_date=timezone.now().replace(hour=0, minute=0, second=0),
        ).last()
        logs.duty_status = last_sts.status
        if last_dvir is not None:
            logs.dvir = last_dvir.defact
        if (
            DriverStatus.objects.filter(driver=driver, logs=logs).count() > 1
            and last_sts.odometer != first.odometer
        ):
            logs.distance = last_sts.odometer - first.odometer
        elif last is not None and last.odometer != first.odometer:
            logs.distance = last.odometer - first.odometer
        else:
            pass
        if last is not None:
            duration = last_sts.cr_time - last.cr_time
            last.finished_time = last_sts.cr_time
            last.dur = duration.total_seconds()
            last.save()
        logs.save()
        return instance


class DriverLimitSerializer(ModelSerializer):
    class Meta:
        fields = ["untilbreak", "shift", "driving_limit", "cycle"]
        model = LogsModels


class DriverStatusSerializer2(ModelSerializer):
    class Meta:
        exclude = ["update_time", "edited"]
        model = DriverStatus

    def update(self, instance, validated_data):
        logs = LogsModels.objects.get(id=validated_data.get("logs").id)
        sts = DriverStatus.objects.filter(id=self.context["id"]).last()
        last_sts = DriverStatus.objects.filter(
            driver=validated_data.get("driver"), logs=logs
        ).last()
        if sts.id == last_sts.id:
            logs.duty_status = validated_data.get("status")
            logs.save()

        super().update(instance, validated_data)
        instance.edited = True
        instance.dur = (
            validated_data.get("finished_time") - validated_data.get("cr_time")
        ).total_seconds()
        instance.save()
        prev = DriverStatus.objects.filter(id__lt=instance.id).order_by("-id").first()
        nex = DriverStatus.objects.filter(id__gt=instance.id).order_by("id").first()

        if prev is not None:
            prev.finished_time = validated_data.get("cr_time")
            prev.dur = (validated_data.get("cr_time") - prev.cr_time).total_seconds()
            prev.save()
        if nex is not None:
            nex.cr_time = validated_data.get("finished_time")
            nex.dur = (
                nex.finished_time - validated_data.get("finished_time")
            ).total_seconds()
            nex.save()
        return instance


class DriverShortSerializer(ModelSerializer):
    driver_status = serializers.StringRelatedField(many=True)

    class Meta:
        model = DRIVERS
        fields = ["id", "name", "last_name", "username", "email", "driver_status"]


class DriverGetSerializer(ModelSerializer):
    class Meta:
        model = DRIVERS
        fields = ["id", "driver_license_number", "name", "last_name"]


class TruckGetSerializer(ModelSerializer):
    class Meta:
        model = Vehicle
        fields = ["model", "vehicle_id"]


class LiveDataShortSerializer(ModelSerializer):
    driver = DriverShortSerializer("driver")
    truck = serializers.StringRelatedField(read_only=True)
    current_status = serializers.SerializerMethodField("current")

    class Meta:
        fields = [
            "id",
            "speed",
            "driver",
            "country",
            "point",
            "truck",
            "current_status",
        ]
        read_only_fields = ["current_status"]
        model = LiveData

    def current(self, obj):
        status = (
            DriverStatus.objects.filter(driver=obj.driver).order_by("cr_time").last()
        )
        return status.status


class LiveDataGetSerializer(ModelSerializer):
    driver = DriverGetSerializer("driver")
    truck = TruckGetSerializer("truck")

    class Meta:
        fields = [
            "vin",
            "engine_state",
            "engine_hours",
            "odometer",
            "point",
            "driver",
            "truck",
            "country",
        ]
        model = LiveData


class DriverVehicleSerializer(ModelSerializer):
    class Meta:
        fields = [
            "id",
            "vehicle_id",
            "make",
            "model",
            "vin",
            "license_plate_num",
            "license_plate_issue_state",
        ]
        model = Vehicle


class TrailerSerializer(ModelSerializer):
    class Meta:
        fields = ["id", "trailer_number"]
        model = Trailer

    def create(self, validated_data):
        trailer = Trailer(
            trailer_number=validated_data["trailer_number"],
            company=validated_data["company"],
        )
        trailer.save()
        driver = DRIVERS.objects.get(email=self.context["request"].user.email)
        driver.trail_number.add(trailer)
        driver.save()
        return trailer


class GeneralMainSerializer(ModelSerializer):
    class Meta:
        fields = [
            "id",
            "distance",
            "shipping_doc",
            "vehicles",
            "trailers",
            "carrier",
            "main_ofice",
            "home_terminal_address",
            "co_driver",
            "from_address",
            "to_address",
            "notes",
            "sign",
            "day",
        ]
        model = GeneralMain


class DVIRSerializer(ModelSerializer):
    class Meta:
        exclude = ["driver", "today_log"]
        model = DVIR


class DriverShort(ModelSerializer):
    class Meta:
        fields = ["username", "vehicle_id"]
        model = DRIVERS


class DVIRSerializer2(ModelSerializer):
    driver = DriverShort("driver", read_only=True)

    class Meta:
        fields = ["driver", "unit", "trailer", "defact", "notes", "time", "sign"]
        model = DVIR


class SignSerializer(ModelSerializer):
    class Meta:
        exclude = ["driver"]
        model = Sign


class LocalEventDataSerializer(ModelSerializer):
    class Meta:
        exclude = [
            "driver",
        ]
        model = LocalEventDatas


class ApkVersionSerializer(ModelSerializer):
    class Meta:
        exclude = [
            "driver",
        ]
        model = ApkVersion


class EldConfimationSerializer(ModelSerializer):
    class Meta:
        exclude = [
            "driver",
        ]
        model = EldConfimation

    def create(self, validated_data):
        driver = DRIVERS.objects.get(email=self.context["request"].user.email)
        eld_of_vehicle = driver.vehicle_id.eld_id
        if (
            str(validated_data["eld_serial"]) == eld_of_vehicle.serial_number
            and str(validated_data["eld_version"]) == eld_of_vehicle.version
        ):
            confir = EldConfimation(
                driver=driver,
                eld_serial=validated_data["eld_serial"],
                eld_version=validated_data["eld_version"],
                similarity=True,
            )
            confir.save()
            return confir
        else:
            confir = EldConfimation(
                driver=driver,
                eld_serial=validated_data["eld_serial"],
                eld_version=validated_data["eld_version"],
                similarity=False,
            )
            confir.save()

            return confir


class LocalEventShortSerializer(ModelSerializer):
    driver = DriverGetSerializer("driver")

    class Meta:
        exclude = [
            "company",
        ]
        model = LocalEventsShort


class ShareLiveDataSerializer(ModelSerializer):
    class Meta:
        model = ShareLiveData
        fields = "__all__"


class ShareLiveDataSerializer2(ModelSerializer):
    data_id = LiveDataShortSerializer(read_only=True)

    class Meta:
        model = ShareLiveData
        fields = ["data_id", "order", "email", "expire"]


class InspectionPdfSer(ModelSerializer):
    class Meta:
        model = InspectionPdf
        fields = "__all__"
