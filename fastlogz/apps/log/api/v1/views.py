import datetime
from datetime import timedelta

# import pdfkit
from django.utils import timezone
# from django.core.mail import EmailMessage
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.views import APIView
from fastlogz.apps.eld.models import DRIVERS

from fastlogz.apps.event.models import DVIR, DriverStatus, GeneralMain, Sign
from fastlogz.apps.event.serializers import (
    DriverStatusSerializer,
    DVIRSerializer,
    GeneralMainSerializer,
    SignSerializer,
)
from fastlogz.apps.log.serializers import (
    LogSerializer,
    LogsModels,
    ReportFile,
    ReportSerializer,
)

# from email import encoders
# from email.mime.base import MIMEBase
# from email.mime.multipart import MIMEMultipart


# from system.settings.base import EMAIL_HOST_USER


# import img2pdf


class Log(APIView):
    queryset = LogsModels.objects.all()

    def get(self, request):
        objects = LogsModels.objects.all()
        serialize = LogSerializer(objects, many=True)
        return Response(serialize.data)


class LogFilter(generics.ListAPIView):
    queryset = LogsModels.objects.all()
    serializer_class = LogSerializer

    def get_queryset(self):
        days = self.kwargs['days']
        time_threshold = timezone.now() - timedelta(days=int(days))
        queryset = LogsModels.objects.filter(
            created__range=(time_threshold, timezone.now()))
        return queryset


class LogFiterByDate(generics.ListAPIView):
    queryset = LogsModels.objects.all()
    serializer_class = LogSerializer

    def get_queryset(self):
        if self.request.GET['from'] and self.request.GET['to']:
            info1 = self.request.GET['from']
            info2 = self.request.GET['to']
            froo = (str(info1).split('.'))
            to = (str(info2).split('.'))
            date1 = datetime.datetime(int(froo[2]), int(froo[1]), int(froo[0]))
            date2 = datetime.datetime(int(to[2]), int(to[1]), int(to[0]))
            if date1 == date2:
                date2 = datetime.datetime(int(to[2]), int(to[1]), int(to[0]), hour=23, minute=59)
                queryset = LogsModels.objects.filter(created__range=(date1, date2))
            else:
                queryset = LogsModels.objects.filter(created__range=(date1, date2))
            return queryset


class LogDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = LogsModels.objects.all()
    serializer_class = LogSerializer
    lookup_field = 'id'


class ReportLogDatasApiView(APIView):
    queryset = LogsModels.objects.all()
    ua = 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36'

    def get(self, request, id):
        queryset = LogsModels.objects.get(id=id)
        serializer = ReportSerializer(queryset, read_only=True)
        return Response(serializer.data)

    def post(self, request, id):
        request.data['id_log'] = id
        serializer = ReportFile(data=request.data)
        # if serializer.is_valid():
        #     a = serializer.save()
        #     email = EmailMessage(
        #         'Reported Daily Log',
        #         'Here is reported log document file ',
        #         EMAIL_HOST_USER,
        #         [serializer.data['email']]
        #     )
        #     # open the file in bynary
        #     img_path = f"{a.pdf_file.path}"

        #     pdf_path = f"reported_files/{id}_{datetime.datetime.now()}.pdf"

        #     # opening image
        #     image = Image.open(img_path)

        #     # converting into chunks using img2pdf
        #     pdf_bytes = img2pdf.convert(image.filename)

        #     # opening or creating pdf file
        #     file = open(pdf_path, "wb")

        #     # writing pdf files with chunks
        #     file.write(pdf_bytes)

        #     # closing image file
        #     image.close()

        #     # closing pdf file
        #     file.close()
        #     pdfname = pdf_path
        #     binary_pdf = open(pdfname, 'rb')
        #     payload = MIMEBase('application', 'octate-stream', Name=pdfname)
        #     payload.set_payload((binary_pdf).read())
        #     # enconding the binary into base64
        #     encoders.encode_base64(payload)
        #     # add header with pdf name
        #     payload.add_header('Content-Decomposition', 'attachment', filename=pdfname)
        #     email.attach(payload)
        #     email.send()
        #     return Response({"data": "File have been sent!"})

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class GeneralDataList(generics.ListAPIView):
    queryset = GeneralMain.objects.all()
    serializer_class = GeneralMainSerializer

    def get_queryset(self):
        time_threshold = timezone.now() - timedelta(days=8)
        queryset = GeneralMain.objects.filter(
            created_date__range=(time_threshold, timezone.now()))
        return queryset


class SignDataList(generics.ListAPIView):
    queryset = Sign.objects.all()
    serializer_class = SignSerializer

    def get_queryset(self):
        time_threshold = timezone.now() - timedelta(days=8)
        queryset = Sign.objects.filter(
            created_date__range=(time_threshold, timezone.now()))
        return queryset


class DvirDataList(generics.ListAPIView):
    queryset = DVIR.objects.all()
    serializer_class = DVIRSerializer

    def get_queryset(self):
        time_threshold = timezone.now() - timedelta(days=8)
        queryset = DVIR.objects.filter(
            created_date__range=(time_threshold, timezone.now()))
        return queryset


class DriverStatusDataList(generics.ListAPIView):
    queryset = DriverStatus.objects.all()
    serializer_class = DriverStatusSerializer

    def get_queryset(self):
        time_threshold = timezone.now() - timedelta(days=8)
        queryset = DriverStatus.objects.filter(
            driver=DRIVERS.objects.filter(email=self.request.user.email).first(),
            cr_time__range=(time_threshold, timezone.now()))
        return queryset
