from django.urls import path

from fastlogz.apps.log.api.v1 import views

app_name = "log"

urlpatterns = [
    path("<int:id>", views.LogDetailView().as_view()),
    path("", views.Log().as_view()),
    path("filter/days/<int:days>", views.LogFilter().as_view()),
    path("filter/", views.LogFiterByDate().as_view()),
    path("report_get/<int:id>", views.ReportLogDatasApiView.as_view()),
]
