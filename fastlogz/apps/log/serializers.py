from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from fastlogz.apps.eld.models import DRIVERS, Vehicle
from fastlogz.apps.eld.serializers import (
    DriverSerializer,
    EldSerializer,
    VehicleSerializer,
)
from fastlogz.apps.event.models import DriverStatus
from fastlogz.apps.event.serializers import GeneralMainSerializer

from .models import LogsModels, ReportLogs


class LogSerializer(ModelSerializer):
    driver = DriverSerializer('driver')
    vehicle = VehicleSerializer('vehicle')

    class Meta:
        model = LogsModels
        fields = '__all__'


class VehicleSerializer(serializers.ModelSerializer):
    eld_id = EldSerializer(read_only=True)

    class Meta:
        model = Vehicle
        exclude = ["notes_vehicle", "device_version", "status", 'company']


class DriverSerializer(serializers.ModelSerializer):
    class Meta:
        model = DRIVERS
        fields = ['id', 'name', 'last_name', 'username', 'driver_license_number', 'dr_li_issue_state']


class DriverStatusSerializer2(ModelSerializer):
    class Meta:
        exclude = ['update_time', 'point']
        model = DriverStatus


class ReportSerializer(ModelSerializer):
    general_info = GeneralMainSerializer(read_only=True, many=True)
    driver = DriverSerializer(read_only=True)
    vehicle = VehicleSerializer(read_only=True)
    logs = DriverStatusSerializer2(read_only=True, many=True)

    class Meta:
        model = LogsModels
        fields = '__all__'


class ReportFile(ModelSerializer):
    class Meta:
        model = ReportLogs
        exclude = ["url"]
