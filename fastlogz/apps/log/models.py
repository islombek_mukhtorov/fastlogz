from django.db import models


class LogsModels(models.Model):
    mychoices = (
        ("OFF", "OFF"),
        ("ON", "ON"),
        ("SB", "SB"),
        ("D", "D")
    )
    driver = models.ForeignKey('eld.DRIVERS', on_delete=models.CASCADE)
    vehicle = models.ForeignKey('eld.Vehicle',  on_delete=models.CASCADE)
    distance = models.FloatField(blank=True, null=True)
    untilbreak = models.FloatField(default=28800)
    driving_limit = models.FloatField(default=39600)
    shift = models.FloatField(default=50400)
    cycle = models.FloatField(default=252000)
    worked_hours = models.FloatField(blank=True, null=True)
    current_status = models.FloatField(blank=True, null=True)  # currrent status da qancha vaqtdan beri kelyatgani
    errors = models.TextField(blank=True, null=True)
    dvir = models.TextField(blank=True, null=True)
    duty_status = models.CharField(max_length=50, choices=mychoices, null=True)
    driving = models.BooleanField(blank=True, default=False)
    cycle_tomorrow = models.FloatField(blank=True, null=True)
    driving_in_violation = models.FloatField(blank=True, null=True)  # violation da yurgan vaqti
    created = models.DateTimeField(auto_now_add=True)
    date_cr = models.DateField(auto_now_add=True)


class ReportLogs(models.Model):
    id_log = models.ForeignKey(LogsModels, on_delete=models.SET_NULL, null=True)
    email = models.EmailField()
    pdf_file = models.FileField(null=True, blank=True)
    url = models.CharField(max_length=200, blank=True, null=True)
