from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from fastlogz.apps.eld import models, serializers


class CompanyProfile(APIView):
    queryset = models.Company.objects.all()

    def get_object(self, request):
        try:
            return models.Company.objects.get(email=request.user)
        except models.Company.DoesNotExist:
            return "error"

    def get(self, request):
        company_profile = self.get_object(request)
        if company_profile == "error":
            return Response(
                {"status": "error", "cause": "you have not enough permission!"}
            )
        else:
            serializer = serializers.CompanySerializer(company_profile)

        return Response(serializer.data)

    def post(self, request):
        company = self.get_object(request)
        user = get_user_model()
        man = user.objects.get(id=request.user.id)
        email = request.data.get("email")
        if email and (email is not man.email):
            if str(email).startswith('"') or str(email).startswith("'"):
                email = email[1, -1]
            man.email = email
        if request.data.get("password") and request.data.get("old_password"):
            if not man.check_password(request.data.get("old_password")):
                return Response(
                    {"response": "Old password is incorrect!"},
                    status=status.HTTP_400_BAD_REQUEST,
                )
            if not man.check_password(request.data.get("password")):
                man.password = make_password(request.data["password"])
            else:
                return Response(
                    {"response": "New password can not be same as old one."},
                    status=status.HTTP_400_BAD_REQUEST,
                )

        serializer = serializers.CompanySerializer(
            company, data=request.data, partial=True
        )
        if serializer.is_valid():
            man.save()
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors)


class UpdateProfileImage(APIView):
    queryset = models.Company.objects.all()

    def get(self, request):
        try:
            model = models.Company.objects.get(email=request.user)
            return Response({"status": "ok", "image": model.image.image.url})
        except ObjectDoesNotExist:
            raise ObjectDoesNotExist

    def post(self, request):
        try:
            user = get_user_model().objects.get(email=request.user)
            user.image = request.data.get("image", 0)
            user.save()
            return Response({"status": "ok", "image": user.image.url})
        except ObjectDoesNotExist:
            return Response({"status": "error"})
